package assign.etl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import assign.domain.Project;
//import assign.domain.UTCourse;






import java.util.logging.*;

public class DBLoader {
	private SessionFactory sessionFactory;
	
	Logger logger;
	
	public DBLoader() {
		// A SessionFactory is set up once for an application
		sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
        
        logger = Logger.getLogger("EavesdropReader");
	}
	
	
	public void loadData(Map<String, Integer> data, String source) throws Exception {
		logger.info("Inside loadData.");
		
		// Transform data into Meeting object to be loaded into DB
		String project_name;
		int count;
		
		for(Map.Entry<String, Integer> entry : data.entrySet()) {
			project_name = entry.getKey();
			project_name = project_name.substring(0, project_name.length()-1);
			
			count = entry.getValue();
			
			Project newProject = new Project(project_name, count);
			addProject(newProject);
			
		}
	}
	
	public Long addProject(Project m) throws Exception {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		Long id = null;
		try {
			tx = session.beginTransaction();
			session.save(m);
		    id = m.getId();
		    tx.commit();
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
				throw e;
			}
		}
		finally {
			session.close();			
		}
		return id;
	}
	
}

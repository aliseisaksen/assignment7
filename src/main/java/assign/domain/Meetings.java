package assign.domain;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "projects")
public class Meetings {
	
	private List<Project> projects = new ArrayList<Project>();
	
	@XmlElement
	private Integer count = null;
	
	
	public List<Project> getProjects() {
		return projects;
	}

	@XmlElement(name="meeting")
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	
	
	public Integer getCount() {
		return count;
	}
	
	public void setCount(int c) {
		this.count = c;
	}
	
}
package assign.services;

import assign.domain.Project;
import assign.domain.Meetings;

public interface EavesdropService {

	//public void EavesdropServiceImpl(String dbUrl, String username, String password);
	
	public Meetings getProjectsFromDB() throws Exception;
	
	// read/GET from db
	public Project getMeeting(String meeting_name, String year) throws Exception;
	
	public Meetings getMeetingByNameAndYear(String team_meeting_name, String year) throws Exception;
	
	public Project getMeeting(int id) throws Exception;
	
}

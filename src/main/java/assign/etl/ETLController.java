package assign.etl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ETLController {
	
	EavesdropReader reader;
	Transformer transformer;
	EavesdropReader meetingReader;
	DBLoader loader;
	
	public ETLController() {
		transformer = new Transformer();
		loader = new DBLoader();
	}
	
	public static void main(String[] args) {
		ETLController etlController = new ETLController();
		
		/* ===============================================================
		 * 	TO LOAD TABLE: Run as Java Application with create property in
		 *  hibernate.cfg.xml after dropping table
		 * ===============================================================
		 */
		
		String source = "http://eavesdrop.openstack.org/meetings/";
		etlController.performETLActions(source);
	}

	public void performETLActions(String source) {		
		try {
			
			reader = new EavesdropReader(source);
			
			// Read data
			Map<String, List<String>> data = reader.readData();
			
			
			// Transform data
			Map<String, List<String>> transformedData = transformer.transform(data, source);
			
			
			
			
			
			System.out.println();
			System.out.println("THIS IS WHAT I WANT" + transformedData.toString());
			
			Map<String, Integer> finalMap = new LinkedHashMap<String, Integer>(); // should contain { 3rd_party_ci/=[.log.html], next=[.log.html] ... }
			
			for(Map.Entry<String, List<String>> entry : transformedData.entrySet()) {
				
				System.out.println("ENTRY: "+ entry.toString()); // 3rd_party_ci/=[2014/]
				String meetingNameKey = entry.getKey();
				
				int count = 0;
				for (Iterator<String> iterator = entry.getValue().iterator(); iterator.hasNext();) {
				    String url = source + meetingNameKey + iterator.next(); // url = http://eavesdrop.openstack.org/meetings/3rd_party_ci/2014/
				    
				    System.out.println("****" + url); // http://eavesdrop.openstack.org/meetings/3rd_party_ci/2014/
				    
				    meetingReader = new EavesdropReader(url);
				    
				    List<String> meetingData = meetingReader.readData2();
				    System.out.println("meeting data:" + meetingData.toString());
				    
			

				    List<String> transformedMeetingData = transformer.transform2(meetingData, meetingNameKey);

				  
				    count += transformedMeetingData.size();
				    finalMap.put(meetingNameKey, count);
				    
				    System.out.println("FINAL MAP:" + finalMap.toString());
				    
//				    System.out.println("FIXED Data: " + transformedMeetingData.toString());

				}
//				System.out.println("*************");
//				System.out.println("");
//				System.out.println("meetingNameKey: " + meetingNameKey + ", count: " + count);
//				System.out.println("");
//				System.out.println("*************");
			}
			
			
			
			
			
			
			
			// Load data
			loader.loadData(finalMap, source);
			

		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}

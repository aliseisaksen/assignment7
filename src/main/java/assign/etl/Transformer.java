package assign.etl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.*;

import org.apache.commons.io.FilenameUtils;

public class Transformer {
	
	Logger logger;
	
	public Transformer() {
		logger = Logger.getLogger("Transformer");		
	}
	
	
	public Map<String, List<String>> transform(Map<String, List<String>> data, String source) {
		
		logger.info("Inside transform.");
		Map<String, List<String>> newData = new HashMap<String, List<String>>();

		String extension = "";
		
		// Find  names of all the files subject to priority order
		for(Map.Entry<String, List<String>> entry : data.entrySet()) {
			
			String key = entry.getKey();
//			int extVal = 0;
//		
//			for (String value : entry.getValue()) {
//				
//				if(value.contains(".log.html"))
//					extVal = 4;
//				else if(value.contains(".log.txt"))
//					if(extVal < 3)
//						extVal = 3;
//				else if(value.contains(".html"))
//					if(extVal < 2)
//						extVal = 2;
//				else if(value.contains(".txt"))
//					if(extVal < 1)
//						extVal = 1;
//			}
//			
//			switch(extVal) {
//				case 4: extension = ".log.html"; break;
//				case 3: extension = ".log.txt"; break;
//				case 2: extension = ".html"; break;
//				case 1: extension = ".txt"; break;
//				default: System.out.println("No appropriate file extension found."); break;
//			}
			
//			int index = 0;
//			for (Iterator<String> iterator = entry.getValue().iterator(); iterator.hasNext();) {
//			    String fileName = iterator.next();
//			    if (!fileName.contains(extension)) {
//			        iterator.remove();
//			    } else {
//			    	// generate absolute link using filename
//			    	data.get(key).set(index, source + key + fileName);
//			    	index++;
//			    }
//			}

		}
		
		newData = data; // data = { project_name/ = [ urlYear1, urlYear2... ] }
		
		return newData;
	}
	
	public List<String> transform2(List<String> data, String meetingName) {
		
		logger.info("Inside transform.");
		
		// CREATE A NEW EMPTY LIST AND ADD TO IT:
		List<String> newData = new ArrayList<String>();

		
		String fileNameSub; // = data.get(0).substring(0, meetingName.length() + 16); // cinder.2015-03-25-16.00
		String extension = "";
		int extVal = 0;
	
		
		String tempName = null;
		String meetingFile = null;
		boolean bool = false;
		// Find  names of all the files subject to priority order
		if(data.size() == 1)
			bool = true;
		
		
		Iterator<String> iterator = data.iterator();
		if(data.size() != 0)
			meetingFile = iterator.next();
		while (iterator.hasNext() || bool) {
			
				
			
//			meetingFile = iterator.next();
			System.out.println("Current MeetingFile: " + meetingFile);
			
			fileNameSub = meetingFile.substring(0, meetingName.length() + 16);
			System.out.println("Current Substring: " + fileNameSub);
			extVal = 0;
			
			System.out.println();
			while(meetingFile.contains(fileNameSub)) {
				
				System.out.println(meetingFile);
				
				if(meetingFile.contains(".log.html")) {
					extVal = 4;
					tempName = meetingFile;
				}
				else if(meetingFile.contains(".log.txt"))
					if(extVal < 3) {
						extVal = 3;
						tempName = meetingFile;
					}
				else if(meetingFile.contains(".html"))
					if(extVal < 2) {
						extVal = 2;
						tempName = meetingFile;
					}
				else if(meetingFile.contains(".txt"))
					if(extVal < 1) {
						extVal = 1;
						tempName = meetingFile;
					}
				
				
				if(iterator.hasNext()) {
					meetingFile = iterator.next();
					System.out.println("============MeetingFile: " + meetingFile);
					if(!meetingFile.contains(fileNameSub)) {
						fileNameSub = meetingFile.substring(0, meetingName.length() + 16);
						extVal = 0;
						break;
					}
				} else {
					break;
				}
				
			}
			bool = false;
			System.out.println();
				
			System.out.println("Ext Val: " + extVal);
			System.out.println("tempName: " + tempName);
				
//			switch(extVal) {
//				case 4: extension = ".log.html"; break;
//				case 3: extension = ".log.txt"; break;
//				case 2: extension = ".html"; break;
//				case 1: extension = ".txt"; break;
//				default: System.out.println("No appropriate file extension found."); break;
//			}
			
				newData.add(tempName);
				
//					int index = 0;
					
//					Iterator<String> iterator = data.iterator();
//					while (iterator.hasNext()) {
						
//						String fileName = iterator.next();
//					    if (!meetingFile.contains(extension)) {
//					        System.out.println("REMOVE: " + meetingFile);
					    	
					    	//iterator.remove();
//					    } else {
					    	// generate absolute link using filename
//					    	data.get(key).set(index, fileName);
//					    	System.out.println("KEPT: " + meetingFile);
//					    	index++;
				System.out.println("End of outer while loop.");
				System.out.println();
				System.out.println("----------------------------------------------------");
				System.out.println();
				}
					
		
				
				//update substring
//				fileNameSub = data.get(0).substring(0, meetingName.length() + 16);
				
//				System.out.println(extVal);
				
//				System.out.println(index);
			
//			extVal = 0;
	
		
		System.out.println("NEW DATA: " + newData.toString());

			
			
//		newData = data; // data = { project_name/ = [ urlYear1, urlYear2... ] }
		
		return newData;
	}
}
package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Meetings;
import assign.domain.Project;
import assign.services.EavesdropService;
import assign.services.EavesdropServiceImpl;

@Path("/myeavesdrop")
public class EavesdropResource {

	EavesdropService eavesdropService;
	String password;
	String username;
	String dburl;
	
	
	public EavesdropResource(@Context ServletContext servletContext) {		
		dburl = servletContext.getInitParameter("DBURL");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		this.eavesdropService = new EavesdropServiceImpl(dburl, username, password);
	}

	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		return "<strong>Hello worldddd </strong>";
	}
	
	/* ===============================================================
	 * 					GET - ASSIGNMENT7
	 * ===============================================================
	 */
	@GET
	@Path("/meetings")
	@Produces("text/html")
	public String getProject(InputStream is) throws Exception {

		System.out.println("Inside /meetings");
		
		
		final Meetings meetings = eavesdropService.getProjectsFromDB();
		
		// 404 Not Found
		if(meetings.getProjects() == null)
			throw new NotFoundException();
		String response = "<table border='1' style='width:100%'><tr><td><strong>Project</strong></td><td><strong>Number of meetings</strong></td></tr>";
			List<Project> projects = meetings.getProjects();
			for(Project p : projects) {
				
				String str = "<tr><td>" + p.getProjectName() + "</td><td>" + p.getCount() + "</td><tr>";					
						
				response = response + str;
			}
//		  <tr>
//		    <td>Jill</td>
//		    <td>Smith</td> 
//		    <td>50</td>
//		  </tr>
//		  <tr>
//		    <td>Eve</td>
//		    <td>Jackson</td> 
//		    <td>94</td>
//		  </tr>
		response = response + "</table>";
		
		
		// Format output
//		meetings.setCount(meetings.getProjects().size());
//		System.out.println(meetings.getProjects().size());
//		meetings.setProjects(null);
	
//		return new StreamingOutput() {
//        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
//        	outputMeetings(outputStream, meetings);
////        	meetings.toString();
//        }
//		};
		
		return response;
	}
	
	
	
	
	
	
	
	/* ===============================================================
	 * 					GET - Count meetings
	 * ===============================================================
	 */
	@GET
	@Path("/meetings/{meeting}/year/{year}/count")
	@Produces("application/xml")
	public StreamingOutput countProject(@PathParam("meeting") String team_meeting_name, @PathParam("year") String year, InputStream is) throws Exception {

		// Error checks on input
		if( !( (team_meeting_name.equals("solum")) || (team_meeting_name.equals("solum_team_meeting")) ) )
			throw new BadRequestException();
		
		if( !( (year.equals("2013")) || (year.equals("2014")) || (year.equals("2015")) ) )
			throw new BadRequestException();
			
		final Meetings meetings = eavesdropService.getMeetingByNameAndYear(team_meeting_name, year);
		
		// 404 Not Found
		if(meetings.getProjects() == null)
			throw new NotFoundException();

		// Format output
		meetings.setCount(meetings.getProjects().size());
		System.out.println(meetings.getProjects().size());
		meetings.setProjects(null);
	
		return new StreamingOutput() {
        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        	outputMeetings(outputStream, meetings);
        }
		};
	}
	
	
	/* ===============================================================
	 * 					GET - Read meetings
	 * ===============================================================
	 */
	@GET
	@Path("/meetings/{meeting}/year/{year}")
	@Produces("application/xml")
	public StreamingOutput readProject(@PathParam("meeting") String team_meeting_name, @PathParam("year") String year, InputStream is) throws Exception {

		// Error checks on input
		if( !( (team_meeting_name.equals("solum")) || (team_meeting_name.equals("solum_team_meeting")) ) )
			throw new BadRequestException();
		
		if( !( (year.equals("2013")) || (year.equals("2014")) || (year.equals("2015")) ) )
			throw new BadRequestException();
			
		final Meetings meetings = eavesdropService.getMeetingByNameAndYear(team_meeting_name, year);
		
		// 404 Not Found
		if(meetings.getProjects() == null)
			throw new NotFoundException();
		if(meetings.getProjects().size() == 0)
			throw new NotFoundException();
		
		return new StreamingOutput() {
        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        	outputMeetings(outputStream, meetings);
        }
		};
	}
		
	
	/* ===============================================================
	 * 						Output Meeting
	 * ===============================================================
	 */
	protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(meetings, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	
}

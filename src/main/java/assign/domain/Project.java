package assign.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table( name = "projects" )
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
	
	@XmlAttribute
	private Long id;
	
	@XmlElement
	private String project_name;
	@XmlElement
	private int count;
	
    public Project() {
    	// this form used by Hibernate
    }
    
    
    public Project(String project_name, int count) {
    	// for application use, to create new events
    	this.project_name = project_name;
    	this.count = count;
    }
    
    
    @Id    
	@GeneratedValue(generator="increment")    
	@GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
		return id;
    }

    private void setId(Long id) {
		this.id = id;
    }
    
    
    @Column(name="project_name")
    public String getProjectName() {
		return project_name;
    }

    public void setProjectName(String project_name) {
		this.project_name = project_name;
    }
    
    
    @Column(name="count")
	public int getCount() {
    	return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
    
}
